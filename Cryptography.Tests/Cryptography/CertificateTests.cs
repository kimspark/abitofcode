﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Cryptography.Core.Certificate;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.Tests.Cryptography
{
    [TestClass]
    public class CertificateTests
    {
        const string CertName = "XXX";
        private const string SerialNumber = "xx ae 27 cc xx 02 00 00 36 xx";

        [TestMethod]
        public void FindCertificateBySerialNumber()
        {
            var found = CertificateOperator.FindCertificate(X509FindType.FindBySerialNumber, 
                SerialNumber);
            Assert.IsNotNull(found);
        }

        [TestMethod]
        public void FindCertificateByName()
        {
            var found = CertificateOperator.FindCertificate(X509FindType.FindBySubjectName,
                CertName, false);
            Assert.IsNotNull(found);
        }

        [TestMethod]
        public void FindWrongCertificate()
        {
            var CertName = "WrongCert";
            var messages = new List<string>();
            var found = CertificateOperator.FindCertificate(X509FindType.FindBySubjectName, 
                CertName);
            Assert.IsNull(found);
        }

        [TestMethod]
        public void SavePublicKey()
        {
            var result = CertificateOperator.SavePublicKeyToFile(CertName, "Pk");
            Assert.AreEqual("", result);
        }

        [TestMethod]
        public void GetMedicinaPublicKey()
        {
            var result = CertificateOperator.GetPublicKeyAsString("XXX");
            Assert.IsNotNull(result);
        }

    }
}
