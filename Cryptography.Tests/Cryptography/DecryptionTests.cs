﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Cryptography.Core.Cryptography;
using Cryptography.Tests.TestDataCreators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.Tests
{
    [TestClass]
    public class DecryptionTests
    {
        const string CertName = "XXX";
        const string ToEncrypt = "Client";

        [TestMethod]
        public void DecryptByGoodCertificate()
        {
            //подготовка зашифрованного документа
            var xmlDoc = XmlTestCreator.CreateByName();
            var enctyptor = new XmlCryptographer();
            enctyptor.EncryptByCertName(xmlDoc, new[] { ToEncrypt }, CertName, false);

            //проверяем, что есть зашифрованные данные
            var encrElem = xmlDoc.SelectSingleNode($"//{ToEncrypt}");
            Assert.IsNull(encrElem);

            enctyptor.Decrypt(xmlDoc);
            var toElem = xmlDoc.SelectSingleNode($"//{ToEncrypt}") as XmlElement;
            Assert.IsNotNull(toElem);
            Assert.AreEqual("Очень секретные персональные данные.", toElem.InnerText);
        }
    }
}
