﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Xml;
using Cryptography.Core.Cryptography;
using Cryptography.Tests.Model;
using Cryptography.Tests.TestDataCreators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Cryptography.Core.Certificate;

namespace Cryptography.Tests.Cryptography
{
    [TestClass]
    public class EncryptionTests
    {
        const string CertName = "XXX";
        const string ToEncrypt = "Clients";

        [TestMethod]
        public void EncryptByGoodCertificate()
        {
            var xmlDoc = XmlTestCreator.CreateByName();

            var enctyptor = new XmlCryptographer();
            var result = enctyptor.EncryptByCertName(xmlDoc, new[] { ToEncrypt }, CertName);

            Assert.AreEqual(ExecutionStatus.Success, result.Status);
            Assert.IsNull(result.Message);

            var hideElem = xmlDoc.SelectSingleNode("//Clients") as XmlElement;
            Assert.IsNull(hideElem);
            var fromElem = xmlDoc.SelectSingleNode("//From") as XmlElement;
            Assert.IsNotNull(fromElem);
            Assert.AreEqual("XXX", fromElem.InnerText);
            var toElem = xmlDoc.SelectSingleNode("//To") as XmlElement;
            Assert.IsNotNull(toElem);
            Assert.AreEqual("YYY", toElem.InnerText);
        }

        [TestMethod]
        public void EncryptByPublicKey()
        {
            var xmlDoc = XmlTestCreator.CreateByName();
            var cert = CertificateOperator.FindCertificate(X509FindType.FindBySubjectName, CertName, false);
            var pk = cert.Export(X509ContentType.Cert);

            var str = JsonConvert.SerializeObject(pk);


            var enctyptor = new XmlCryptographer();
            var result = enctyptor.EncryptByPublicKey(xmlDoc, new[] {ToEncrypt}, pk, false);

            Assert.AreEqual(ExecutionStatus.Success, result.Status);
            Assert.IsNull(result.Message);

            var hideElem = xmlDoc.SelectSingleNode("//Clients") as XmlElement;
            Assert.IsNull(hideElem);
            var fromElem = xmlDoc.SelectSingleNode("//From") as XmlElement;
            Assert.IsNotNull(fromElem);
            Assert.AreEqual("XXX", fromElem.InnerText);
            var toElem = xmlDoc.SelectSingleNode("//To") as XmlElement;
            Assert.IsNotNull(toElem);
            Assert.AreEqual("YYY", toElem.InnerText);
        }

        [TestMethod]
        public void EncryptByBadCertificate()
        {
            var xmlDoc = XmlTestCreator.CreateByName();

            var enctyptor = new XmlCryptographer();
            var result = enctyptor.EncryptByCertName(xmlDoc, new[] { ToEncrypt }, 
                "BadCertificate", false);

            Assert.AreEqual(ExecutionStatus.Failed, result.Status);
            Assert.IsNotNull(result.Message);

            var hideElem = xmlDoc.SelectSingleNode("//Clients") as XmlElement;
            Assert.IsNotNull(hideElem);
        }

        [TestMethod]
        public void EncryptByService()
        {
            var xmlDoc = XmlTestCreator.CreateByName();

            var values = new Dictionary<string, string>
            {
                {    "CertName", CertName },
                {    "ToEncryptElements", ToEncrypt },
                {    "Document", xmlDoc.InnerXml }
            };

            var content = new FormUrlEncodedContent(values);

            var client = new HttpClient();
            var responseRaw = client.PostAsync(
                "http://xxx", content).Result;

            var customerJsonString = responseRaw.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<CryptographyResponse>(customerJsonString);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var resXml = new XmlDocument();
            resXml.LoadXml(response.ResultDocument);

            var hideElem = resXml.SelectSingleNode("//Client") as XmlElement;
            Assert.IsNull(hideElem);

            var fromElem = resXml.SelectSingleNode("//From") as XmlElement;
            Assert.IsNotNull(fromElem);
            Assert.AreEqual("XXX", fromElem.InnerText);

            var toElem = xmlDoc.SelectSingleNode("//To") as XmlElement;
            Assert.IsNotNull(toElem);
            Assert.AreEqual("YYY", toElem.InnerText);
        }

        [TestMethod]
        public void EncryptByServiceBadCertificate()
        {
            var xmlDoc = XmlTestCreator.CreateByName();

            var values = new Dictionary<string, string>
            {
                {    "CertName", "BadCertificate" },
                {    "ToEncryptElements", ToEncrypt },
                {    "Document", xmlDoc.InnerXml }
            };

            var content = new FormUrlEncodedContent(values);

            var client = new HttpClient();
            var responseRaw = client.PostAsync(
                "http://test.encryption.renins.com/cryptograpy/EncryptDocument", content).Result;

            var customerJsonString = responseRaw.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<CryptographyResponse>(customerJsonString);

            Assert.AreEqual(HttpStatusCode.InternalServerError, response.StatusCode);
            Assert.IsNull(response.ResultDocument);
            Assert.AreEqual("Certificate not found.", response.ReasonPhrase);
        }

        [TestMethod]
        public void EncryptByServiceBadNode()
        {
            var xmlDoc = XmlTestCreator.CreateByName();

            var values = new Dictionary<string, string>
            {
                {    "CertName", CertName },
                {    "ToEncryptElements", ToEncrypt },
                {    "Document", xmlDoc.InnerXml }
            };

            var content = new FormUrlEncodedContent(values);

            var client = new HttpClient();
            var responseRaw = client.PostAsync(
                "http://xxx", content).Result;

            var customerJsonString = responseRaw.Content.ReadAsStringAsync().Result;
            var response = JsonConvert.DeserializeObject<CryptographyResponse>(customerJsonString);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

            var resXml = new XmlDocument();
            resXml.LoadXml(response.ResultDocument);

            var hideElem = resXml.SelectSingleNode("//Client") as XmlElement;
            Assert.IsNull(hideElem);

            var fromElem = resXml.SelectSingleNode("//From") as XmlElement;
            Assert.IsNotNull(fromElem);
            Assert.AreEqual("XXX", fromElem.InnerText);

            var toElem = xmlDoc.SelectSingleNode("//To") as XmlElement;
            Assert.IsNotNull(toElem);
            Assert.AreEqual("YYY", toElem.InnerText);
        }

        [TestMethod]
        public void EncryptAndCompare()
        {
            var smallDoc = XmlTestCreator.CreateByName();
            var bigDoc = XmlTestCreator.CreateBigXml();

            var enctyptor = new XmlCryptographer();
            enctyptor.EncryptByCertName(smallDoc, new[] { ToEncrypt }, CertName, false);
            enctyptor.EncryptByCertName(bigDoc, new[] { ToEncrypt }, CertName, false);

            var smallSize = smallDoc.InnerXml.Length;
            var bigSize = bigDoc.InnerXml.Length;

            Assert.IsTrue(smallSize < bigSize);

        }
    }
}
