﻿using System;
using System.Text;
using System.Xml;
using Cryptography.Core.Cryptography;
using Cryptography.Core.ECP;
using Cryptography.Tests.TestDataCreators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Cryptography.Tests.ECP
{
    [TestClass]
    public class EcpTest
    {
        const string CertName = "XXX";
        const string ToEncrypt = "Clients";

        [TestMethod]
        public void SignByEcp()
        {
            var xmlDoc = XmlTestCreator.CreateByName();

            var signer = new XmlSigner();
            signer.Sign(xmlDoc, CertName, false);
                
            Assert.AreEqual(ExecutionStatus.Success,signer.VerifyXml(xmlDoc).Status);

            var encrElem = xmlDoc.SelectSingleNode("//Clients") as XmlElement;
            Assert.IsNotNull(encrElem);
            var fromElem = xmlDoc.SelectSingleNode("//From") as XmlElement;
            Assert.IsNotNull(fromElem);
            Assert.AreEqual("XXX", fromElem.InnerText);
            var toElem = xmlDoc.SelectSingleNode("//To") as XmlElement;
            Assert.IsNotNull(toElem);
            Assert.AreEqual("YYY", toElem.InnerText);         
        }

        [TestMethod]
        public void VerifyXmlFile()
        {
            // Создаем новый XML документ в памяти.
            var xmlDocument = new XmlDocument {PreserveWhitespace = true};
            // Загружаем подписанный документ из файла.
            xmlDocument.Load("signed_file.xml");

            var signer = new XmlSigner();
            var result = signer.VerifyXml(xmlDocument);

            Assert.AreEqual(ExecutionStatus.Success, result.Status);
        }

       [TestMethod]
        public void EncryptAndSignByEcp()
        {
            var xmlDoc = XmlTestCreator.CreateByName();

            var enctyptor = new XmlCryptographer();
            enctyptor.EncryptByCertName(xmlDoc, new[] { ToEncrypt }, CertName, false);

            var signer = new XmlSigner();
            signer.Sign(xmlDoc, CertName, false);

            Assert.AreEqual(ExecutionStatus.Success, signer.VerifyXml(xmlDoc).Status);

            var encrElem = xmlDoc.SelectSingleNode("//Clients") as XmlElement;
            Assert.IsNull(encrElem);
            var fromElem = xmlDoc.SelectSingleNode("//From") as XmlElement;
            Assert.IsNotNull(fromElem);
            Assert.AreEqual("XXX", fromElem.InnerText);
            var toElem = xmlDoc.SelectSingleNode("//To") as XmlElement;
            Assert.IsNotNull(toElem);
            Assert.AreEqual("YYY", toElem.InnerText);
        }

        [TestMethod]
        public void DecryptSignedXml()
        {
            // Создаем новый XML документ в памяти.
            var xmlDocument = new XmlDocument { PreserveWhitespace = true };
            // Загружаем подписанный документ из файла.
            xmlDocument.Load("encrypted_and_signed_file.xml");

            var enctyptor = new XmlCryptographer();            
            enctyptor.Decrypt(xmlDocument);

            //проверяем, что успешно расшифровалось
            var toElem = xmlDocument.SelectSingleNode($"//{ToEncrypt}") as XmlElement;
            Assert.IsNotNull(toElem);
            Assert.AreEqual("Очень секретные персональные данные.", toElem.InnerText);
        }
    }
}
