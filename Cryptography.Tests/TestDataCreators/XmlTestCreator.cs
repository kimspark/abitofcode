﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Cryptography.Tests.TestDataCreators
{
    public static class XmlTestCreator
    {
        // Тестовый документ для зашифрования / расшифрования.
        private static string SourceDocument = "" +
            "<MyXML Signed=\"false\">" +
                "<Message Signed=\"true\" Id=\"obj\">" +
                "    <Clients Count=\"645\">" +
                        "Очень секретные персональные данные.</Clients>" +
                "    <From>" +
                        "XXX</From>" +
                "    <To>" +
                        "YYY</To>" +
                "</Message>" +
            "</MyXML>";

        // Тестовый документ для шифрования / расшифрования.
        private static string BigXml = "" +
            "<MyXML Signed=\"false\">" +
                "<Message Signed=\"true\" Id=\"obj\">" +
                "    <Clients Count=\"645\">" +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные." +
                        "Очень секретные персональные данные.</Clients>" +
                "    <From>" +
                        "XXX</From>" +
                "    <To>" +
                        "YYY</To>" +
                "</Message>" +
            "</MyXML>";

        public static XmlDocument CreateByName()
        {
            var document = new XmlDocument();
            document.LoadXml(SourceDocument);
            return document;
        }

        public static XmlDocument CreateBigXml()
        {
            var document = new XmlDocument();
            document.LoadXml(BigXml);
            return document;
        }

    }
}
