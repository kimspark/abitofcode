﻿using System.Net;
using System.Web.Http;
using System.Xml;
using Cryptography.Core.Cryptography;
using CryptographyServices.Model;
using Cryptography.Core.Managers;
using Cryptography.Core.Extensions;

namespace CryptographyServices.Controllers
{
    public class EcpController : ApiController
    {
        private ICryptographyManager manager;

        public EcpController()
        {
            manager = new CryptographyManager();
        }

        /// <summary>Сервис подписи ЭЦП для XML (с сохранением подписи внутри файла)</summary>
        /// <param name="doc">документ на подпись</param>
        /// <returns></returns>
        [HttpPost]
        public CryptographyResponse SignXmlDocument([FromBody]XmlDocument doc)
        {
            var result = manager.SignXmlDocument(doc, "CryptographyServices");
            var response = FillResponse(result);

            return response;
        }

        /// <summary>Сервис подписи ЭЦП для XML (с сохранением подписи внутри файла)</summary>
        /// <param name="doc">документ на подпись</param>
        /// <param name="toEncrypt">какие узлы шифровать</param>
        /// <param name="recieveCertName">имя сертификата получателя</param>
        /// <returns></returns>
        [HttpPost]
        [Route("SignAndEncryptXmlDocument")]
        public CryptographyResponse SignAndEncryptXmlDocument(EncryptData request)
        {
            var result = manager.SignAndEncryptXmlDocument(request, "CryptographyServices");
            var response = FillResponse(result);

            return response;
        }

        private CryptographyResponse FillResponse(CryptographyResult result)
        {
            var response = new CryptographyResponse();
            response.StatusCode = result?.Status.ToHttpStatus() ?? HttpStatusCode.InternalServerError;
            response.ReasonPhrase = result?.Message;
            response.ResultDocument = result?.Document?.InnerXml;

            return response;
        }
    }
}