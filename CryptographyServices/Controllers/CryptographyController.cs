﻿using System.Net;
using System.Web.Http;
using Cryptography.Core.Cryptography;
using CryptographyServices.Model;
using Cryptography.Core.Managers;
using Cryptography.Core.Extensions;

namespace CryptographyServices.Controllers
{
    public class CryptographyController : ApiController
    {
        private ICryptographyManager manager;

        public CryptographyController()
        {
            manager = new CryptographyManager();
        }

        /// <summary> Зашифровать документ </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("EncryptDocument")]
        public CryptographyResponse EncryptDocument([FromBody]CryptographyData request)
        {
            var result = manager.EncryptDocument(request, "CryptographyServices");
            var response = FillResponse(result);

            return response;
        }

        /// <summary> Зашифровать документ </summary>
        /// <param name="companyName">Название комании, чей пубилчный ключ нужно извлечь из крипто-сервера</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetActualPk")]
        public string GetActualPk(string companyName)
        {
            return manager.GetActualPk(companyName, "CryptographyServices");
        }

        /// <summary>Расшифровать документ</summary>
        /// <param name="request">запрос</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DecryptDocument")]
        public CryptographyResponse DecryptDocument([FromBody]CryptographyData request)
        {
            var result = manager.DecryptDocument(request?.Document, "CryptographyServices");
            var response = FillResponse(result);

            return response;
        }

        private CryptographyResponse FillResponse(CryptographyResult result)
        {
            var response = new CryptographyResponse
            {
                StatusCode = result?.Status.ToHttpStatus() ?? HttpStatusCode.InternalServerError,
                ReasonPhrase = result?.Message,
                ResultDocument = result?.Document?.InnerXml
            };

            return response;
        }
    }
}