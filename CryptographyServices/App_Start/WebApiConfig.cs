﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace CryptographyServices
{
    /// <summary>Конфигурация роутинга.</summary>
    public class WebApiConfig
    {
        /// <summary> регистрация </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
        }
    }
}