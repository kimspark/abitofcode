﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace CryptographyServices.Model
{
    public class CryptographyRequest
    {
        /// <summary>Документ для шифрования </summary>
        public string Document { get; set; }

        /// <summary>Элементы для шифрования</summary>
        public string[] ToEncryptElements { get; set; }

        /// <summary>Имя сертификата, которым нужно зашифровать</summary>
        public string CertName { get; set; }

        /// <summary>Сертификат, которым нужно зашифровать</summary>
        public byte[] Certificate { get; set; }
    }
}