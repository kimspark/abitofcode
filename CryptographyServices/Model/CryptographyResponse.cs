﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Xml;

namespace CryptographyServices.Model
{
    public class CryptographyResponse 
    {
        public string ReasonPhrase { get; set; }
        public string ResultDocument { get; set; }

        public HttpStatusCode StatusCode { get; set; }
    }
}