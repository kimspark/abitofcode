﻿using Cryptography.Core.Cryptography;
using System.Net;

namespace Cryptography.Core.Extensions
{
    public static class ExecutionStatusExt
    {
        public static HttpStatusCode ToHttpStatus(this ExecutionStatus status)
        {
            return status == ExecutionStatus.Success ? HttpStatusCode.OK :
                   status == ExecutionStatus.BadRequest ? HttpStatusCode.BadRequest :
                   HttpStatusCode.InternalServerError;
        }
    }
}