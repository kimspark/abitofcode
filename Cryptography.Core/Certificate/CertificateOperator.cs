﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using Newtonsoft.Json;

namespace Cryptography.Core.Certificate
{
    public static class CertificateOperator
    {
        /// <summary>Найти сертификат по заданным параметрам</summary>
        /// <param name="findType">тип поиска сертификата</param>
        /// <param name="param">значение параметра поиска</param>
        /// <param name="valid">признак валидности сертификата (по-умолчанию, валидный)</param>
        /// <returns></returns>
        public static X509Certificate2 FindCertificate(X509FindType findType, string param,
            bool valid = true)
        {
            var store = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);
            var found = store.Certificates.Find(findType, param, valid);

            switch (found.Count)
            {
                case 0:
                    throw new Exception("Certificate not found.");
                default:
                    return found[0];
            }
        }

        /// <summary>
        /// Найти сертификат по имени и сохранить в файл
        /// </summary>
        /// <param name="param"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string SavePublicKeyToFile(string param, string fileName)
        {
            try
            {
                var cert = FindCertificate(X509FindType.FindBySubjectName, param, false);
                var pk = cert.Export(X509ContentType.Cert);
                var sPk = System.Text.Encoding.UTF8.GetString(pk, 0, pk.Length);

                using (var outputFile = new StreamWriter(fileName))
                {
                    outputFile.Write(sPk);
                }
            }
            catch (Exception exception)
            {
                return exception.Message;
            }
           
            return "";
        }

        public static string GetPublicKeyAsString(string param)
        {
            try
            {
                var cert = FindCertificate(X509FindType.FindBySubjectName, param, false);
                var pk = cert.Export(X509ContentType.Cert);
                return JsonConvert.SerializeObject(pk);
            }
            catch (Exception)
            {
                return null;
            }           
        }
    }
}