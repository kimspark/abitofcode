﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Cryptography.Core.Cryptography;

namespace Cryptography.Core.ECP
{
    public interface ISigner
    {
        /// <summary>Подписать документ ЭЦП</summary>
        /// <param name="doc">документ</param>
        /// <param name="keyName">секретный ключ</param>
        /// <param name="byValidCert"></param>
        /// <returns></returns>
        CryptographyResult Sign(XmlDocument doc, string keyName, bool byValidCert = true);
    }
}
