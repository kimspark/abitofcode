﻿using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;
using Cryptography.Core.Certificate;
using Cryptography.Core.Cryptography;
using CryptoPro.Sharpei.Xml;

namespace Cryptography.Core.ECP
{
    public class XmlSigner : ISigner
    {
        public CryptographyResult Sign(XmlDocument doc, string certName, bool byValidCert = true)
        {
            var result = new CryptographyResult(ExecutionStatus.Success, document: doc);

            //Ищем сертификат получателя
            var certificate = CertificateOperator.FindCertificate(
            X509FindType.FindBySubjectName, certName, byValidCert);

            // Открываем ключ подписи.
            var key = certificate.PrivateKey;
            // Подписываем созданный XML файл и сохраняем.
            SignXmlNode(doc, key, certificate);

            return result;
        }

        /// <summary>Проверка подписи под XML документом.</summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public CryptographyResult VerifyXml(XmlDocument doc)
        {
            var result = new CryptographyResult();

            // Сохраняем все пробельные символы, они важны при проверке подписи.
            doc.PreserveWhitespace = true;

            // Создаем объект SignedXml для проверки подписи документа.
            var signedXml = new SignedXml(doc);

            // Ищем все node "Signature" и сохраняем их в объекте XmlNodeList
            var nodeList = doc.GetElementsByTagName(
                "Signature", SignedXml.XmlDsigNamespaceUrl);

            // Загружаем первую подпись в SignedXml
            signedXml.LoadXml((XmlElement)nodeList[0]);

            // Проверяем подпись.
            result.Document = doc;
            result.Status = signedXml.CheckSignature() ? ExecutionStatus.Success
                : ExecutionStatus.Failed;

            return result;
        }

        /// <summary>Подписывание XML файла и сохранение подписи в новом файле.</summary>
        /// <param name="doc"></param>
        /// <param name="key"></param>
        /// <param name="certificate"></param>
        private void SignXmlNode(XmlDocument doc, AsymmetricAlgorithm key,
            X509Certificate certificate)
        {
            // Пробельные символы участвуют в вычислении подписи и должны быть сохранены для совместимости с другими реализациями.
            doc.PreserveWhitespace = true;

            // Создаем объект SignedXml по XML документу и добавляем ключ в SignedXml документ. 
            var signedXml = new SignedXml(doc) { SigningKey = key };

            // Создаем ссылку на node для подписи
            // Явно проставляем алгоритм хеширования,
            // по умолчанию SHA1.
            var reference = new Reference
            {
                Uri = "#obj",
                DigestMethod = CPSignedXml.XmlDsigGost3411Url
            };


            // Добавляем ссылку в объект SignedXml.
            signedXml.AddReference(reference);

            // Создаем объект KeyInfo.
            var keyInfo = new KeyInfo();

            // Добавляем сертификат в KeyInfo
            keyInfo.AddClause(new KeyInfoX509Data(certificate));

            // Добавляем KeyInfo в SignedXml.
            signedXml.KeyInfo = keyInfo;

            // Можно явно проставить алгоритм подписи: ГОСТ Р 34.10.
            // Если сертификат ключа подписи ГОСТ Р 34.10
            // и алгоритм ключа подписи не задан, то будет использован
            // XmlDsigGost3410Url
            // signedXml.SignedInfo.SignatureMethod =
            //     CPSignedXml.XmlDsigGost3410Url;

            // Вычисляем подпись.
            signedXml.ComputeSignature();

            // Получаем XML представление подписи и сохраняем его 
            // в отдельном node.
            var xmlDigitalSignature = signedXml.GetXml();

            // Добавляем элемент в XML документ.
            doc.DocumentElement.AppendChild(doc.ImportNode(xmlDigitalSignature, true));
        }
    }
}