﻿using Blockchain.Cryptography;

namespace Blockchain.Managers
{
    public interface IBlockchainApiManager
    {
        /// <summary>
        /// <summary> Метод получения публичного ключа </summary>
        /// </summary>
        /// <param name="inn">ИНН партнера</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>результат получения публичного ключа</returns>
        PartnerPublicKeyResult GetPartnerPublicKey(string inn, string serviceName = null);

        /// <summary>
        /// Метод записи участника в блокчейн
        /// </summary>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>результат записи участника в блокчейн</returns>
        PartnerResult PostPartner(string serviceName = null);
    }
}