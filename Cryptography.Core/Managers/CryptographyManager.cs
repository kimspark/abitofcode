﻿using System;
using System.Xml;
using Cryptography.Core.Certificate;
using Renins.Logger.Wrapper;
using Renins.Logger.WCFModel;
using Cryptography.Core.ECP;
using Cryptography.Core.Cryptography;

namespace Cryptography.Core.Managers
{
    public class CryptographyManager : ICryptographyManager
    {
        private const string CertName = "XXX"; //взять потом из конфигов;
        private readonly LoggerWrapper _log = LW.Instance;

        /// <summary>
        /// <summary> Метод шифрования документа </summary>
        /// </summary>
        /// <param name="cryptoData">Данные для шифрования</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>результат шифрования документа</returns>
        public CryptographyResult EncryptDocument(CryptographyData cryptoData, string serviceName = null)
        {
            const string method = nameof(SignXmlDocument);
            CryptographyResult result;
            var msg = _log.CreateInfoMessage(method, $"Метод {method} успешно выполнен").
                Add(MessageParams.ServiceName, serviceName).
                Add(MessageParams.MethodName, method);

            try
            {
                var certComment = cryptoData?.Certificate == null ? "Certificate is empty" : "Certificate is not empty";
                var encryptElementsComent = GetEncryptElementsComent(cryptoData?.EncryptElements);
                msg.Add(MessageParams.Request, $"EncryptElements {encryptElementsComent}, " +
                                               $"\nCertificateName {cryptoData?.CertName}, \n{certComment}");
                if (cryptoData?.Document == null)
                {
                    throw new ArgumentException("Передан пустой документ");
                }
                if (cryptoData.EncryptElements == null)
                {
                    throw new ArgumentException("Должен быть передан список элементов для шифрования");
                }
                if (string.IsNullOrEmpty(cryptoData.CertName) && (cryptoData.Certificate == null))
                {
                    throw new ArgumentException("Должны быть указаны либо название сертификата, " +
                                                "либо публичный ключ для шифрования");
                }

                var documentXml = new XmlDocument();
                documentXml.LoadXml(cryptoData?.Document);
                msg.Add(MessageParams.Xml, documentXml);

                var enctyptor = new XmlCryptographer();
                if (cryptoData?.Certificate == null)
                {
                    result = enctyptor.EncryptByCertName(documentXml, cryptoData?.EncryptElements,
                        cryptoData?.CertName);
                }
                else
                {
                    result = enctyptor.EncryptByPublicKey(documentXml, cryptoData.EncryptElements,
                        cryptoData?.Certificate);
                }

                msg.Add(MessageParams.Response, $"Status {result?.Status}, \nMessage {result?.Message}");
                msg.Add(MessageParams.Xml, result?.Document);
            }
            catch (Exception exception)
            {
                var status = exception is ArgumentException ? ExecutionStatus.BadRequest : ExecutionStatus.Failed;
                result = new CryptographyResult(status, exception.Message);

                msg.MessageType = MessageTypeCode.Error;
                msg.AdditionalInfo = $"Ошибка при выполнении метода {method}";
                msg.Add(MessageParams.ErrorDetails, exception.Message).
                    Add(MessageParams.StackTrace, exception.StackTrace);
            }
            finally
            {
                msg.WriteAsync();
            }

            return result;
        }

        public string GetActualPk(string companyName, string serviceName = null)
        {
            try
            {
                return CertificateOperator.GetPublicKeyAsString(companyName);
            }
            catch (Exception)
            {
                return "";
            }               
        }

        /// <summary>
        /// Метод дешифрования документа
        /// </summary>
        /// <param name="document">Документ для дешифрования</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>результат дешифрования документа</returns>
        public CryptographyResult DecryptDocument(string document, string serviceName = null)
        {
            const string method = nameof(SignXmlDocument);
            CryptographyResult result;
            var msg = _log.CreateInfoMessage(method, $"Метод {method} успешно выполнен").
                          Add(MessageParams.ServiceName, serviceName).
                          Add(MessageParams.MethodName, method);

            try
            {
                if (document == null)
                {
                    throw new ArgumentException("Передан пустой документ");
                }

                var documentXml = new XmlDocument();
                documentXml.LoadXml(document);
                msg.Add(MessageParams.Xml, documentXml);

                var enctyptor = new XmlCryptographer();
                result = enctyptor.Decrypt(documentXml);

                msg.Add(MessageParams.Response, $"Status {result?.Status}, \nMessage {result?.Message}");
                msg.Add(MessageParams.Xml, result?.Document);
            }
            catch (Exception exception)
            {
                var status = exception is ArgumentException ? ExecutionStatus.BadRequest : ExecutionStatus.Failed;
                result = new CryptographyResult(status, exception.Message);

                msg.MessageType = MessageTypeCode.Error;
                msg.AdditionalInfo = $"Ошибка при выполнении метода {method}";
                msg.Add(MessageParams.ErrorDetails, exception.Message).
                    Add(MessageParams.StackTrace, exception.StackTrace);
            }
            finally
            {
                msg.WriteAsync();
            }

            return result;
        }

        /// <summary>
        /// Метод подписи ЭЦП для XML (с сохранением подписи внутри файла)
        /// </summary>
        /// <param name="document">документ на подпись</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>Результат подписи документа</returns>
        public CryptographyResult SignXmlDocument(XmlDocument document, string serviceName = null)
        {
            const string method = nameof(SignXmlDocument);
            CryptographyResult result;
            var msg = _log.CreateInfoMessage(method, $"Метод {method} успешно выполнен").
                          Add(MessageParams.ServiceName, serviceName).
                          Add(MessageParams.MethodName, method);

            try
            {
                if (document == null)
                {
                    throw new ArgumentException("Передан пустой документ");
                }

                msg.Add(MessageParams.Xml, document);

                var signer = new XmlSigner();
                result = signer.Sign(document, CertName, false);

                msg.Add(MessageParams.Response, $"Status {result?.Status}, \nMessage {result?.Message}");
                msg.Add(MessageParams.Xml, result?.Document);
            }
            catch (Exception exception)
            {
                var status = exception is ArgumentException ? ExecutionStatus.BadRequest : ExecutionStatus.Failed;
                result = new CryptographyResult(status, exception.Message);

                msg.MessageType = MessageTypeCode.Error;
                msg.AdditionalInfo = $"Ошибка при выполнении метода {method}";
                msg.Add(MessageParams.ErrorDetails, exception.Message).
                    Add(MessageParams.StackTrace, exception.StackTrace);
            }
            finally
            {
                msg.WriteAsync();
            }

            return result;
        }

        /// <summary>
        /// Метод подписи ЭЦП для XML (с сохранением подписи внутри файла)
        /// </summary>
        /// <param name="encryptData">Данные для шифрования</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>Результат подписи документа</returns>
        public CryptographyResult SignAndEncryptXmlDocument(EncryptData encryptData, string serviceName = null)
        {
            const string method = nameof(SignAndEncryptXmlDocument);
            CryptographyResult result;
            var msg = _log.CreateInfoMessage(method,
                $"Метод {method} успешно выполнен").
                          Add(MessageParams.ServiceName, serviceName).
                          Add(MessageParams.MethodName, method);

            try
            {
                if (encryptData?.Document == null)
                {
                    throw new ArgumentException("Передан пустой документ");
                }

                var encryptElementsComent = GetEncryptElementsComent(encryptData.EncryptElements);
                msg.Add(MessageParams.Request, $"EncryptElements {encryptElementsComent}, " +
                                               $"\nCertificateName {encryptData.CertName}");
                msg.Add(MessageParams.Xml, encryptData.Document);

                var enctyptor = new XmlCryptographer();
                result = enctyptor.EncryptByCertName(encryptData.Document, encryptData.EncryptElements, 
                    encryptData.CertName, false);

                if (result.Status == ExecutionStatus.Success)
                {
                    var signer = new XmlSigner();
                    result = signer.Sign(result.Document, CertName, false);
                }

                msg.Add(MessageParams.Response, $"Status {result?.Status}, \nMessage {result?.Message}");
                msg.Add(MessageParams.Xml, result?.Document);
            }
            catch (Exception exception)
            {
                var status = exception is ArgumentException ? ExecutionStatus.BadRequest : ExecutionStatus.Failed;
                result = new CryptographyResult(status, exception.Message);

                msg.MessageType = MessageTypeCode.Error;
                msg.AdditionalInfo = $"Ошибка при выполнении метода {method}";
                msg.Add(MessageParams.ErrorDetails, exception.Message).
                    Add(MessageParams.StackTrace, exception.StackTrace);
            }
            finally
            {
                msg.WriteAsync();
            }

            return result;
        }

        private string GetEncryptElementsComent(string[] encryptElements)
        {
            return encryptElements == null ? "" : string.Join(",", encryptElements);
        }
    }
}