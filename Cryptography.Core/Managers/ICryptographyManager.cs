﻿using System.Xml;
using Cryptography.Core.Cryptography;

namespace Cryptography.Core.Managers
{
    public interface ICryptographyManager
    {
        /// <summary>
        /// <summary> Метод шифрования документа </summary>
        /// </summary>
        /// <param name="cryptoData">Данные для шифрования</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>результат шифрования документа</returns>
        CryptographyResult EncryptDocument(CryptographyData cryptoData, string serviceName = null);

        /// <summary>
        /// <summary> Метод шифрования документа </summary>
        /// </summary>
        /// <param name="companyName">Название комании, чей пубилчный ключ нужно извлечь из крипто-сервера</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>результат шифрования документа</returns>
        string GetActualPk(string companyName, string serviceName = null);

        /// <summary>
        /// Метод дешифрования документа
        /// </summary>
        /// <param name="document">Документ для дешифрования</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>результат дешифрования документа</returns>
        CryptographyResult DecryptDocument(string document, string serviceName = null);

        /// <summary>
        /// Метод подписи ЭЦП для XML (с сохранением подписи внутри файла)
        /// </summary>
        /// <param name="document">документ на подпись</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>Результат подписи документа</returns>
        CryptographyResult SignXmlDocument(XmlDocument document, string serviceName = null);

        /// <summary>
        /// Метод подписи ЭЦП для XML (с сохранением подписи внутри файла)
        /// </summary>
        /// <param name="encryptData">Данные для шифрования</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>Результат подписи документа</returns>
        CryptographyResult SignAndEncryptXmlDocument(EncryptData encryptData, string serviceName = null);
    }
}