﻿using System;
using Renins.Logger.Wrapper;
using Renins.Logger.WCFModel;
using Blockchain.Cryptography;

namespace Blockchain.Managers
{
    public class BlockchainApiManager : IBlockchainApiManager
    {
        private LoggerWrapper _log = LW.Instance;

        /// <summary>
        /// <summary> Метод получения публичного ключа </summary>
        /// </summary>
        /// <param name="inn">ИНН партнера</param>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>результат получения публичного ключа</returns>
        public PartnerPublicKeyResult GetPartnerPublicKey(string inn, string serviceName = null)
        {
            PartnerPublicKeyResult result;
            var msg = _log.CreateInfoMessage("GetPartnerPublicKey", "Метод GetPartnerPublicKey успешно выполнен").
                          Add(MessageParams.ServiceName, serviceName).
                          Add(MessageParams.MethodName, "GetPartnerPublicKey");

            try
            {
                msg.Add(MessageParams.Request, $"Inn {inn}");

                //здесь будет логика а не просто заглушка
                string pk;
                switch (inn)
                {
                    case "7724023076":
                        pk = "MIIIDzCCB76gAwIBAgIRAOEDbhsH4NSA5xHhNpO8HhQwCAYGKoUDAgIDMIIBIzEYMBYGBSqFA2QBEg0xMTI3NzQ2MDM2NDk0MRowGAYIKoUDA4EDAQESDDAwNzcyMjc2NjU5ODELMAkGA1UEBhMCUlUxHDAaBgNVBAgMEzc3INCzLiDQnNC+0YHQutCy0LAxFTATBgNVBAcMDNCc0L7RgdC60LLQsDE7MDkGA1UECQwy0YPQuy4g0JDQstC40LDQvNC+0YLQvtGA0L3QsNGPLCDQtC4gONCQLCDRgdGC0YAuIDUxUjBQBgNVBAoMSdCX0JDQniAi0J3QsNGG0LjQvtC90LDQu9GM0L3Ri9C5INGD0LTQvtGB0YLQvtCy0LXRgNGP0Y7RidC40Lkg0YbQtdC90YLRgCIxGDAWBgNVBAMMD9CX0JDQniDQndCj0KYgMjAeFw0xNzA1MTIwNjUzMjVaFw0xNzA3MTIwNjUzMjVaMIIBMjEOMAwGA1UEKwwFMDAwMDAxHjAcBgkqhkiG9w0BCQEWD3Rlc3RAcmVuaW5zLmNvbTEoMCYGA1UECwwf0KHRgtGA0LDRhdC+0LLQsNC90LjQtSDQsNCy0YLQvjEPMA0GA1UECgwG0JPQoNChMTgwNgYDVQQJDC/QlNC10YDQsdC10L3QtdCy0YHQutCw0Y8g0L3QsNCxLiDQtC43INGB0YLRgC4yMjEVMBMGA1UEBwwM0JzQvtGB0LrQstCwMRwwGgYDVQQIDBM3NyDQsy4g0JzQvtGB0LrQstCwMQswCQYDVQQGEwJSVTFJMEcGA1UEAwxAItCe0J7QniAi0JPQoNCj0J/Qn9CQINCg0JXQndCV0KHQodCQ0J3QoSDQodCi0KDQkNCl0J7QktCQ0J3QmNCVIjBjMBwGBiqFAwICEzASBgcqhQMCAiQABgcqhQMCAh4BA0MABEA6/k4cDhBy1rcy49L5Oc3eiLPwsRSiOVkdMbrEF++ftftutx3HJqmFzdF++hr/Y0eKX3zcbFWOrD7PQntxxstFo4IEtjCCBLIwDgYDVR0PAQH/BAQDAgP4MEsGA1UdJQREMEIGCCsGAQUFBwMEBgcqhQMCAiIGBggrBgEFBQcDAgYHKoUDAgIiGQYJKoUDA4FLAgEBBgYqhQNkcQEGByqFAwICIgIwggFcBgNVHSMEggFTMIIBT4AUN4I6Hs/iCqVaDgc8rC+i5Gxl74ChggEppIIBJTCCASExGjAYBggqhQMDgQMBARIMMDA3NzEwNDc0Mzc1MRgwFgYFKoUDZAESDTEwNDc3MDIwMjY3MDExHjAcBgkqhkiG9w0BCQEWD2RpdEBtaW5zdnlhei5ydTE8MDoGA1UECQwzMTI1Mzc1INCzLiDQnNC+0YHQutCy0LAg0YPQuy4g0KLQstC10YDRgdC60LDRjyDQtC43MSwwKgYDVQQKDCPQnNC40L3QutC+0LzRgdCy0Y/Qt9GMINCg0L7RgdGB0LjQuDEVMBMGA1UEBwwM0JzQvtGB0LrQstCwMRwwGgYDVQQIDBM3NyDQsy4g0JzQvtGB0LrQstCwMQswCQYDVQQGEwJSVTEbMBkGA1UEAwwS0KPQpiAxINCY0KEg0JPQo9CmggoYPK2PAAMAAAfxMB0GA1UdDgQWBBSYubU5sc5h/lDKwTLKUkzBmUSwJTATBgNVHSAEDDAKMAgGBiqFA2RxATAzBgkrBgEEAYI3FQcEJjAkBhwqhQMCAjIBCcDidYON222EtYtugs/GbYGiGe4eAgEBAgEAMIIBdQYFKoUDZHAEggFqMIIBZgwh0J/QkNCa0JwgItCa0YDQuNC/0YLQvtCf0YDQviBIU00iDIGO0J/RgNC+0LPRgNCw0LzQvNC90L4t0LDQv9C/0LDRgNCw0YLQvdGL0Lkg0LrQvtC80L/Qu9C10LrRgSAi0KPQtNC+0YHRgtC+0LLQtdGA0Y/RjtGJ0LjQuSDRhtC10L3RgtGAICLQmtGA0LjQv9GC0L7Qn9GA0L4g0KPQpiIg0LLQtdGA0YHQuNGPIDIuMAxV0KHQtdGA0YLQuNGE0LjQutCw0YIg0YHQvtC+0YLQstC10YLRgdGC0LLQuNGPINCh0KQvMTIxLTI0MTQg0L7RgiAxNyDQuNGO0L3RjyAyMDE0INCzLgxZ0KHQtdGA0YLQuNGE0LjQutCw0YIg0YHQvtC+0YLQstC10YLRgdGC0LLQuNGPINCh0KQvMTI4LTI4ODEg0L7RgiAxMiDQsNC/0YDQtdC70Y8gMjAxNiDQsy4wJQYFKoUDZG8EHAwa0JrRgNC40L/RgtC+0J/RgNC+IENTUCAzLjkwbwYDVR0fBGgwZjAxoC+gLYYraHR0cDovL3d3dy5uY2FyZi5ydS9kb3dubG9hZC96YW9udWNwYWsyLmNybDAxoC+gLYYraHR0cDovL2NkcC5uY2FyZi5ydS9kb3dubG9hZC96YW9udWNwYWsyLmNybDB5BggrBgEFBQcBAQRtMGswMAYIKwYBBQUHMAGGJGh0dHA6Ly9vY3NwMjAubmNhcmYucnUvb2NzcC9vY3NwLnNyZjA3BggrBgEFBQcwAoYraHR0cDovL3d3dy5uY2FyZi5ydS9kb3dubG9hZC96YW9udWNwYWsyLmNlcjAIBgYqhQMCAgMDQQCvF6h5NPHb/wiRALvmPamKv2se0BfQuF+EZuRPaxVI4G7umpuVbYiiCVmDPMRorn8PucYY9+JPXB6dcG5p+Bhv";
                        break;
                    case "7729058330":
                        pk = "MIIFaDCCBRegAwIBAgIRDwBotwByqHigQlQABPtIhcgwCAYGKoUDAgIDMIGIMSAwHgYJKoZIhvcNAQkBFhFjcGNhQGNyeXB0b3Byby5ydTELMAkGA1UEBhMCUlUxFTATBgNVBAcMDNCc0L7RgdC60LLQsDEjMCEGA1UECgwa0J7QntCeINCa0KDQmNCf0KLQni3Qn9Cg0J4xGzAZBgNVBAMMEtCj0KYgS1DQmNCfVE8t0J9QTzAeFw0xODAxMjQxMDU3NDZaFw0yMzAxMjQxMTA3NDZaMIIBQzEjMCEGCSqGSIb3DQEJARYUZG9nb3ZvcjFAbWVkaWNpbmEucnUxCzAJBgNVBAYTAlJVMRUwEwYDVQQHDAzQnNC+0YHQutCy0LAxIjAgBgNVBAoMGdCe0JDQniAi0JzQtdC00LjRhtC40L3QsCIxQDA+BgNVBAsMN9Ce0YLQtNC10Lsg0L/QviDQvtGE0L7RgNC80LvQtdC90LjRjiDQtNC+0LPQvtCy0L7RgNC+0LIxOzA5BgNVBAMMMtCT0YDQtdGI0LjQu9C+0LLQsCDQnNCw0YDQuNGPINCb0LXQvtC90LjQtNC+0LLQvdCwMVUwUwYDVQQMDEzQndCw0YfQsNC70YzQvdC40Log0L7RgtC00LXQu9CwINC/0L4g0L7RhNC+0YDQvNC70LXQvdC40Y4g0LTQvtCz0L7QstC+0YDQvtCyMGMwHAYGKoUDAgITMBIGByqFAwICJAAGByqFAwICHgEDQwAEQH3muT/EbAAsQ2DQV81Et7VohQNisDzELbU/dA1C560wR+CTeQlRYDx+YEtFpNoDhnGAzy8c0Qdg3JAWwxxdIdajggKaMIICljAOBgNVHQ8BAf8EBAMCBPAwHQYDVR0OBBYEFJ0WrwB9JbGL+Gdrs2SOT+rzFqS0MB8GCSsGAQQBgjcVBwQSMBAGCCqFAwICLgAIAgEBAgEAMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDBDArBgNVHRAEJDAigA8yMDE4MDEyNDEwNTc0NlqBDzIwMTkwMTI0MTA1NzQ2WjCBrgYDVR0fBIGmMIGjME6gTKBKhkhodHRwOi8vY2RwLmNyeXB0b3Byby5ydS9jZHAvNzJmMDUwODZiMjgwOWZhZDAyMzllMGMzOTMxNjBlZTJiM2E3N2EyNi5jcmwwUaBPoE2GS2h0dHA6Ly9jcGNhMjAuY3J5cHRvcHJvLnJ1L2NkcC83MmYwNTA4NmIyODA5ZmFkMDIzOWUwYzM5MzE2MGVlMmIzYTc3YTI2LmNybDB/BggrBgEFBQcBAQRzMHEwNAYIKwYBBQUHMAGGKGh0dHA6Ly9vY3NwLmNyeXB0b3Byby5ydS9vY3NwMjAvb2NzcC5zcmYwOQYIKwYBBQUHMAKGLWh0dHA6Ly9jcGNhMjAuY3J5cHRvcHJvLnJ1L2FpYS9jcGNhMmNhY2VyLnA3YjCBxQYDVR0jBIG9MIG6gBRy8FCGsoCfrQI54MOTFg7is6d6JqGBjqSBizCBiDEgMB4GCSqGSIb3DQEJARYRY3BjYUBjcnlwdG9wcm8ucnUxCzAJBgNVBAYTAlJVMRUwEwYDVQQHDAzQnNC+0YHQutCy0LAxIzAhBgNVBAoMGtCe0J7QniDQmtCg0JjQn9Ci0J4t0J/QoNCeMRswGQYDVQQDDBLQo9CmIEtQ0JjQn1RPLdCfUE+CEQ/dEE7klJC/gOcRvgQFWfH+MAgGBiqFAwICAwNBAPXUWFNHd2y3yBZZcnBCBUHvAihKoI8JSRCCa/oMlJgPiNXlvkM0MxiEUMgQK5lOHjkv4/nwBNY0BNVHskJogLU=";
                        break;
                    default:
                        pk = "";
                        break;
                }

                result = new PartnerPublicKeyResult(ExecutionStatus.Success, publicKey: pk);

                msg.Add(MessageParams.Response, $"Status {result?.Status}, \nPublicKey {result?.PublicKey}, \nMessage {result?.Message}");
            }
            catch (Exception exception)
            {
                result = new PartnerPublicKeyResult(ExecutionStatus.Failed, exception.Message);
                msg.MessageType = MessageTypeCode.Error;
                msg.AdditionalInfo = "Ошибка при выполнении метода GetPartnerPublicKey";
                msg.Add(MessageParams.ErrorDetails, exception.Message).
                    Add(MessageParams.StackTrace, exception.StackTrace);
            }
            finally
            {
                msg.WriteAsync();
            }

            return result;
        }

        /// <summary>
        /// Метод записи участника в блокчейн
        /// </summary>
        /// <param name="serviceName">Имя сервиса</param>
        /// <returns>результат записи участника в блокчейн</returns>
        public PartnerResult PostPartner(string serviceName = null)
        {
            PartnerResult result;
            var msg = _log.CreateInfoMessage("PostPartner", "Метод PostPartner успешно выполнен").
                          Add(MessageParams.ServiceName, serviceName).
                          Add(MessageParams.MethodName, "PostPartner");

            try
            {
                //здесь будет логика а не просто заглушка
                result = new PartnerResult(ExecutionStatus.Success);

                msg.Add(MessageParams.Response, $"Status {result?.Status}, \nMessage {result?.Message}");
            }
            catch (Exception exception)
            {
                result = new PartnerResult(ExecutionStatus.Failed, exception.Message);
                msg.MessageType = MessageTypeCode.Error;
                msg.AdditionalInfo = "Ошибка при выполнении метода PostPartner";
                msg.Add(MessageParams.ErrorDetails, exception.Message).
                    Add(MessageParams.StackTrace, exception.StackTrace);
            }
            finally
            {
                msg.WriteAsync();
            }

            return result;
        }
    }
}