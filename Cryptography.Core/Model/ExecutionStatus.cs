﻿namespace Cryptography.Core.Cryptography
{
    public enum ExecutionStatus
    {
        Success,
        Failed,
        BadRequest
    }
}