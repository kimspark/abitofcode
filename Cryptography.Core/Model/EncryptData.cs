﻿using System.Xml;

namespace Cryptography.Core.Cryptography
{
    public class EncryptData
    {
        /// <summary>Документ для шифрования </summary>
        public XmlDocument Document { get; set; }

        /// <summary>Элементы для шифрования</summary>
        public string[] EncryptElements { get; set; }

        /// <summary>Имя сертификата, которым нужно зашифровать</summary>
        public string CertName { get; set; }
    }
}