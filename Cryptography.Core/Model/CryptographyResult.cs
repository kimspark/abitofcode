﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace Cryptography.Core.Cryptography
{
    public class CryptographyResult
    {
        /// <summary>статус</summary>
        public ExecutionStatus Status { get; set; }

        ///  <summary>сообщение, который вернул метод</summary>
        public string Message { get; set; }

        ///  <summary>документ, который вернул метод</summary>
        public XmlDocument Document;

        public CryptographyResult()
        {

        }

        public CryptographyResult(ExecutionStatus status, string message = null, XmlDocument document = null)
        {
            Status = status;
            Message = message;
            Document = document;
        }
    }
}