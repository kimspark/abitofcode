﻿namespace Cryptography.Core.Cryptography
{
    public class CryptographyData
    {
        /// <summary>Документ для шифрования </summary>
        public string Document { get; set; }

        /// <summary>Элементы для шифрования</summary>
        public string[] EncryptElements { get; set; }

        /// <summary>Имя сертификата, которым нужно зашифровать</summary>
        public string CertName { get; set; }

        /// <summary>Сертификат, которым нужно зашифровать</summary>
        public byte[] Certificate { get; set; }
    }
}