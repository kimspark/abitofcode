﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Cryptography.Core.Cryptography
{
    public interface ICryptographer<T> where T: class
    {
        /// <summary>
        /// Метод шифрует документ и возвращает его
        /// </summary>
        /// <param name="doc">документ, который нужно зашифровать</param>
        /// <param name="elementNames">список элементов, которые нужно зашифровать</param>
        /// <param name="keyName">имя публичного ключа, которым нужно зашифровать</param>
        /// <param name="byValidCert">признак: использовать только валидный сертификат</param>
        /// <returns></returns>
        CryptographyResult EncryptByCertName(T doc, IEnumerable<string> elementNames, 
            string keyName, bool byValidCert = true);

        /// <summary>
        /// Метод дешифрует xml-документ и возвращает его
        /// </summary>
        /// <param name="doc">зашифрованный документ</param>
        /// <param name="keyName">имя секретного ключа, если не указан, то брать из конфигураций</param>
        /// <returns></returns>
        CryptographyResult Decrypt(T doc, string keyName = null);
    }
}
