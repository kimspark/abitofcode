﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Xml;
using CryptoPro.Sharpei;
using Cryptography.Core.Certificate;
using CryptoPro.Sharpei.Xml;

namespace Cryptography.Core.Cryptography
{
    public class XmlCryptographer : ICryptographer<XmlDocument>
    {
        public CryptographyResult EncryptByCertName(XmlDocument doc, IEnumerable<string> nodeNames,
            string cerName, bool byValidCert = true)
        {
            var result = new CryptographyResult(ExecutionStatus.Success, document: doc);
            X509Certificate2 cert;

            //Ищем сертификат получателя
            cert = CertificateOperator.FindCertificate(X509FindType.FindBySubjectName, cerName, byValidCert);
            EncryptByCert(doc, nodeNames, result, cert);

            return result;
        }

        public CryptographyResult EncryptByPublicKey(XmlDocument doc, IEnumerable<string> nodeNames,
            byte[] publicKey, bool byValidCert = true)
        {
            var result = new CryptographyResult(ExecutionStatus.Success, document: doc);
            X509Certificate2 cert;

            cert = new X509Certificate2();
            cert.Import(publicKey);
            EncryptByCert(doc, nodeNames, result, cert);

            return result;
        }

        private void EncryptByCert(XmlDocument doc, IEnumerable<string> nodeNames,
            CryptographyResult result, X509Certificate2 cert)
        {
            // Пробельные символы участвуют в вычислении подписи 
            //и должны быть сохранены для совместимости с другими реализациями
            doc.PreserveWhitespace = true;
            //Шифруем каждый узел из списка
            foreach (var node in nodeNames)
            {
                // Ищем заданный элемент для заширования.
                var elementToEncrypt = doc.SelectSingleNode($"//*[local-name()='{node}']")
                    as XmlElement;
                if (elementToEncrypt == null) //если какого-то узла нет, то пропускаем
                {
                    result.Message = $"Узел {node} не найден";
                    continue;
                }

                var eXml = new EncryptedXml();
                var edElement = eXml.Encrypt(elementToEncrypt, cert);
                EncryptedXml.ReplaceElement(elementToEncrypt, edElement, false);
            }
        }

        public CryptographyResult Decrypt(XmlDocument xmlDoc, string keyName = null)
        {
            var result = new CryptographyResult(ExecutionStatus.Success, document: xmlDoc);

            // Создаем новый объект EncryptedXml по XML документу.
            var exml = new EncryptedXml(xmlDoc);

            // Расшифровываем зашифрованные узлы XML документа.
            exml.DecryptDocument();

            return result;
        }

        public CryptographyResult DecryptWithKey(XmlDocument xmlDoc, string keyName = null)
        {
            var result = new CryptographyResult(ExecutionStatus.Success, document: xmlDoc);

            var nsmgr = new XmlNamespaceManager(xmlDoc.NameTable);
            nsmgr.AddNamespace("enc", "http://www.w3.org/2001/04/xmlenc#");
            var list = xmlDoc.SelectNodes("//enc:EncryptedData", nsmgr);

            // Создаем объект EncryptedXml.
            var exml = new EncryptedXml(xmlDoc);

            if (list == null) return result;

            // Для всех зашифрованных данных.
            foreach (XmlNode node in list)
            {
                var element = node as XmlElement;
                var encryptedData = new EncryptedData();
                if (element != null)
                {
                    encryptedData.LoadXml(element);

                    // Находим подходящий ключ для расшифрования.
                    var decryptionKey = GetDecryptionKey(exml, encryptedData);
                    if (decryptionKey == null)
                    {
                        result.Message = "Ключ для расшифрования сообщения не найден.";
                        result.Status = ExecutionStatus.Failed;
                        break;
                    }

                    // И на нем расшифровываем данные.
                    var decryptedData = exml.DecryptData(encryptedData, decryptionKey);
                    exml.ReplaceData(element, decryptedData);
                }
            }

            return result;
        }

        // Получение ключа расшифрования
        private static SymmetricAlgorithm GetDecryptionKey(EncryptedXml exml,
            EncryptedData encryptedData)
        {
            IEnumerator encryptedKeyEnumerator = encryptedData.KeyInfo.GetEnumerator();
            // Проходим по всем KeyInfo
            while (encryptedKeyEnumerator.MoveNext())
            {
                // пропускам все что неизвестно.
                KeyInfoEncryptedKey current = encryptedKeyEnumerator.Current
                    as KeyInfoEncryptedKey;
                if (current == null)
                    continue;
                // до первого EncryptedKey
                EncryptedKey encryptedKey = current.EncryptedKey;
                if (encryptedKey == null)
                    continue;
                KeyInfo keyinfo = encryptedKey.KeyInfo;
                // Проходим по всем KeyInfo зашифрования ключа.
                IEnumerator srcKeyEnumerator = keyinfo.GetEnumerator();
                while (srcKeyEnumerator.MoveNext())
                {
                    // пропускам все что неизвестно.
                    KeyInfoX509Data keyInfoCert = srcKeyEnumerator.Current
                        as KeyInfoX509Data;
                    if (keyInfoCert == null)
                        continue;
                    Gost3410 myKey = FindMy(keyInfoCert.Certificates);
                    if (myKey == null)
                        continue;
                    return CPEncryptedXml.DecryptKeyClass(encryptedKey.CipherData.CipherValue,
                        myKey, encryptedData.EncryptionMethod.KeyAlgorithm);
                }
            }

            return null;
        }

        private static Gost3410 FindMy(ArrayList certificates)
        {
            X509Store store = new X509Store("My", StoreLocation.CurrentUser);
            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);
            foreach (X509Certificate2 cert in certificates)
            {
                // ищем сертификат в хранилище MY
                int foundN = store.Certificates.IndexOf(cert);
                if (foundN < 0)
                    continue;
                // со ссылкой на секретный ключ.
                AsymmetricAlgorithm alg = store.Certificates[foundN].PrivateKey;
                if (alg == null)
                    continue;
                // и ГОСТ алгоритмом секретного ключа.
                Gost3410 gost = alg as Gost3410;
                if (gost == null)
                    continue;
                return gost;
            }

            return null;
        }

    }
}