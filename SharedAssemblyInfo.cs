using System.Reflection;

[assembly: AssemblyCompany(".")]
[assembly: AssemblyCopyright("Copyright ©  2019")]

// Don't edit manually! Jenkins does it by himself.
[assembly: AssemblyVersion("1.0.75")]
